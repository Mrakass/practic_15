﻿#include <iostream>
using namespace std;

void PrintNumbers(bool isOdd, int N) {
	if (isOdd) {
		for (int i = 1; i <= N; i += 2) {
			cout << i << endl;
		}
	}
	else{
		for (int i = 0; i <= N; i += 2)
			cout << i << endl;

	
	}
}
	
int main(){
	PrintNumbers(false,	10);
	PrintNumbers(true, 10);
	return 0;
	
}
